from __future__ import annotations
import numpy as np
from pathlib import Path
from itertools import product
import toml
from typing import Sequence, Iterable, Iterator, Callable
import sys
from .image import ROI, S, to_photons

_int_type = (int, np.uint8, np.uint16, np.uint32, np.uint64)
_float_type = (float, np.float16, np.float32, np.float64)

def print_through(X):
    print(X)
    return X

class ParamSpec:
    """
    Simple class to hold parameter specifications consisting of a name (str)
    and, optionally, a mapping function and a unit (str).
    """
    def __init__(
        self,
        name: str,
        unit: str=None,
        f: type(lambda float: float)=None,
    ):
        self.name = str(name)
        self.unit = str(unit) if unit is not None else ""
        self.f = (lambda x: x) if f is None else f

    def __eq__(self, other: str) -> bool:
        return other == self.name

class IndexMap:
    """
    Dict-like class with ordered-ness guarantees and additional methods to
    facilitate key access via an index.
    """
    def __init__(self, order: list[str], data: dict[str, ...]):
        self.N = len(order)
        self.labels = {i: k for i, k in enumerate(order)}
        self.vals = data

    @staticmethod
    def from_dict(d: dict[str, ...]):
        return IndexMap(list(d.keys()), d)

    def __getitem__(self, k: int | str) -> ...:
        if isinstance(k, _int_type):
            return self.vals[self.labels[k]]
        elif isinstance(k, str):
            return self.vals[k]
        else:
            raise KeyError("invalid key")

    def get(self, k: int | str, default=None) -> ...:
        if isinstance(k, _int_type):
            return self.vals[self.labels[k]] if k < self.N else default
        elif isinstance(k, str):
            return self.vals.get(k, default)
        else:
            raise TypeError("invalid key type")

    def __setitem__(self, k: str, val: ...):
        if k in self.vals.keys():
            self.vals[k] = val
        else:
            self.labels[self.N] = k
            self.vals[k] = val
            self.N += 1

    def keys(self) -> Iterator[str]:
        for i in range(self.N):
            yield self.labels[i]

    def keys_indexed(self) -> Iterator[tuple[int, str]]:
        for i in range(self.N):
            yield (i, self.labels[i])

    def values(self) -> Iterator[...]:
        for i in range(self.N):
            yield self.vals[self.labels[i]]

    def values_indexed(self) -> Iterator[tuple[int, ...]]:
        for i in range(self.N):
            yield (i, self.vals[self.labels[i]])

    def items(self) -> Iterator[tuple[str, ...]]:
        for i in range(self.N):
            yield (self.labels[i], self.vals[self.labels[i]])

    def items_indexed(self) -> Iterator[tuple[int, str, ...]]:
        for i in range(self.N):
            yield (i, self.labels[i], self.vals[self.labels[i]])

    def __iter__(self) -> Iterator[tuple[int, str, ...]]:
        return self.items_indexed()

def load_params(
    datadir: Path,
    infile: str,
    paramslist: Sequence[ParamSpec],
    extra_params: dict[str, float | Iterable]=None,
) -> tuple[IndexMap[tuple[str, str], np.ndarray], int]:
    """
    Load target parameter names and values from a file, combining with extra
    values not found in the file, if desired. Extra parameters will be
    overridden if their names collide with those found in the file.

    Parameters
    ----------
    datadir : pathlib.Path
        Path to the directory containing the .toml file.
    infile : str
        Filename including the ".toml".
    paramslist : Sequence[ParamSpec]
        Select variables from the driving script, all converted to lower case.
        Should be listed in their scan order.
    extra_params : dict[str, float | Iterable] (optional)
        Additional parameters to record.

    Returns
    -------
    params : IndexMap[str, numpy.ndarray[ndim=1, dtype=numpy.float64]]
        Names and values of parameters of interest.
    reps : int
        Number of repetitions for each implied parameter configuration.
    """
    bad_param = lambda x: Exception(f"bad parameter specification `{x}`")
    with datadir.joinpath(infile).open('r') as infile_data:
        data = toml.load(infile_data)
    params = IndexMap.from_dict({
        k if isinstance(k, tuple) else (k, "")
            : np.array([v]) if not isinstance(v, Iterable) else np.array(v)
        for k, v in extra_params.items()
    } if extra_params is not None else dict())
    for param in paramslist:
        if param.name not in data.keys():
            print(
                f"WARNING: could not find values for parameter {param.name};"
                " ignoring..."
            )
            continue
        params[(param.name, param.unit)] = (
            param.f(np.array([data[param.name]]))
            if not isinstance(data[param.name], Iterable)
            else param.f(np.array(data[param.name]))
        )
    reps = data["reps"]
    return params, reps

class DataSet:
    """
    Class to handle storage and iteration over collected data under sets of N
    scanned parameters. Where applicable, data arrays have the following axis
    structure:
        0: data kind [quantity, std. error] (N/A for per-rep quantities)
        1: ROI index
        2: rep index (N/A for averaged quantities)
        3..N + 2: N axes for N parameters of interest
        N + 3: pre/post index (index = 0 for pre, 1 for post) (N/A for survival)
        N + 4: image i (y) (N/A for summed quantities)
        N + 5: image j (x) (N/A for summed quantities)

    Attributes
    ----------
    params : IndexMap[str, numpy.ndarray[ndim=1, dtype=numpy.float64]]
        Names and values of N parameters of interest. See also `load_params`.
    reps : int
        Number of repetitions for each implied parameter configuration.
    rois : list[image.ROI]
        List of ROIs.
    threshold : float
        Threshold photon count.
    img_avg : numpy.ndarray[ndim=N+3, dtype=numpy.float64]
        Complete images, averaged over reps.
            [ params..., pre/post, img_i, img_j ]
    roi_optim_locs : numpy.ndarray[ndim=N+5, dtype=numpy.float64]
        Optimal ROI locations.
            [ roi, rep, params..., pre/post, <loc> ]
        where <loc> stands for the 2x2 array describing the ROI:
            array([[ x, y ],
                   [ w, h ]])
    roi_img_avg : numpy.ndarray[ndim=N+4, dtype=numpy.float64]
        Per-ROI sub-images, averaged over reps.
            [ roi, params..., pre/post, roi_i, roi_j ]
    roi_totals : numpy.ndarray[ndim=N+4, dtype=numpy.float64]
        Total photon counts within each ROI.
            [ kind, roi, rep, params..., pre/post ]
    mpc : numpy.ndarray[ndim=N+3, dtype=numpy.float64]
        Mean photon count.
            [ kind, roi, params..., pre/post ]
    fat : numpy.ndarray[ndim=N+3, dtype=numpy.float64]
        Fraction above threshold.
            [ kind, roi, params..., pre/post ]
    survival : numpy.ndarray[ndim=N+2, dtype=numpy.float64] or None
        Survival fraction. `None` if data is not pre/post-structured.
            [ kind, roi, params... ]
    """
    params: IndexMap
    reps: int
    rois: list
    threshold: float
    img_avg: np.ndarray
    roi_optim_locs: np.ndarray
    roi_img_avg: np.ndarray
    roi_totals: np.ndarray
    mpc: np.ndarray
    fat: np.ndarray
    survival: np.ndarray | None

    def __init__(
        self,
        params: IndexMap[str, np.ndarray],
        reps: int,
        num_images: int,
        rois: Sequence[ROI],
        threshold: float,
        img_avg: np.ndarray,
        roi_optim_locs: np.ndarray,
        roi_img_avg: np.ndarray,
        roi_totals: np.ndarray,
        mpc: np.ndarray,
        fat: np.ndarray,
        survival: np.ndarray=None,
    ):
        self.params = params
        self.reps = abs(int(reps))
        self.num_images = abs(int(num_images))
        self.rois = list(rois)
        self.threshold = float(threshold)
        self.img_avg = img_avg
        self.roi_optim_locs = roi_optim_locs
        self.roi_img_avg = roi_img_avg
        self.roi_totals = roi_totals
        self.mpc = mpc
        self.fat = fat
        self.survival = survival

    @staticmethod
    def from_raw(
        params: IndexMap[str, np.ndarray],
        reps: int,
        num_images: int,
        raw_photons: np.ndarray,
        rois: Sequence[ROI],
        threshold: float,
        sort_axes: bool=True,
    ):
        """
        Construct from a newly read data array (i.e. a 3D array of camera
        photonelectron counts) with associated scanning parameters for some
        number of ROIs.

        Parameters
        ----------
        params : IndexMap[str, numpy.ndarray[ndim=1, dtype=numpy.float64]
            Names and values of parameters of interest. See also `load_params`.
        reps : int
            Number of repetitions for each implied parameter configureation.
        num_images : int > 0
            Number of images to expect a pre-/post-shot structure in the data
            array.
        raw_photons : numpy.ndarray[ndim=4, dtype=numpy.float64]
            Unshaped data array of photon counts (see `image.load_image` and
            `image.to_photons`).
        rois : Sequence[ROI]
            ROI information. See `image.ROI`.
        threshold : float
            Threshold photon count.
        sort_axes : bool = True (optional)
            Sort the data array and the values in each array in `params`.
        """
        # complete images
        # [quantity/error, rep, pre/post, params..., img_i, img_j]
        master = raw_photons.reshape((
            2,
            reps,
            *[len(X) for X in params.values()],
            abs(int(num_images)),
            *raw_photons.shape[-2:]
        ))
        if sort_axes:
            sort_idx = IndexMap.from_dict(
                {k: np.argsort(v) for k, v in params.items()}
            )
            master = S.sort_idx_nd(
                master,
                tuple(),
                np.arange(2),
                np.arange(reps),
                *list(sort_idx.values()),
            )

        # averaged complete images
        # [params..., pre/post, img_i, img_j]
        img_avg = master[0].mean(axis=0)

        # roi_imgs: optimal ROI sub-images
        # [roi, rep, params..., pre/post, roi_i, roi_j]
        # roi_locs: optimal ROI locations
        # [roi, rep, params..., pre/post, pos/size, xy/wh]
        # roi_imgs_err: standard error for ROI sub-image pixel counts
        # [roi, rep, params..., pre/post, roi_i, roi_j]
        roi_X = list(zip(*[roi.select_optim(master[0], threshold) for roi in rois]))
        roi_imgs = np.array(roi_X[0])
        roi_locs = np.array(roi_X[1])
        roi_imgs_err = np.array([
            roi._select_optim(master[1], locs, len(master[1].shape) - 2)
            for roi, locs in zip(rois, roi_locs)
        ])

        # total photon counts in each ROI sub-image
        # [kind, roi, rep, params..., pre/post]
        _roi_totals = roi_imgs.sum(axis=(-2, -1))
        _roi_totals_err = np.sqrt((roi_imgs_err**2).sum(axis=(-2, -1)))
        roi_totals = np.array([_roi_totals, _roi_totals_err])

        # total photon count averaged over reps
        # [kind, roi, params..., pre/post]
        _mpc = roi_totals[0].mean(axis=1)
        # _mpc_err = roi_totals[0].std(axis=2) / np.sqrt(reps)
        _mpc_err = roi_totals[0].std(axis=1) / np.sqrt(reps)
        # _mpc_err = np.sqrt((roi_totals[1]**2).sum(axis=1)) / reps
        mpc = np.array([_mpc, _mpc_err])

        # fraction above threshold
        # [kind, roi, params..., pre/post]
        _fat = (roi_totals[0] >= threshold).sum(axis=1) / reps
        _fat_err = np.sqrt(_fat / reps)
        fat = np.array([_fat, _fat_err])

        # survival fraction between first and last images
        # (FAT conditioned on being above threshold in pre)
        # [roi, params...] or None if num_images > 1
        if num_images > 1:
            is_pre = roi_totals.T[0].T[0] >= threshold
            N_pre = is_pre.sum(axis=1)
            with np.errstate(divide="ignore", invalid="ignore"):
                # s_test = is_pre * (roi_totals.T[-1].T[0] >= threshold) # new
                # s_test_err = np.std(s_test, axis=1) # new
                surv = (
                    (is_pre * (roi_totals.T[-1].T[0] >= threshold)).sum(axis=1)
                    / N_pre
                )
                surv_err = np.sqrt((surv**2 + surv) / N_pre) # old
                # surv_err = s_test_err / np.sqrt(N_pre) # new
            surv[np.where(np.isnan(surv) + np.isinf(surv))] = -0.05
            surv_err[np.where(np.isnan(surv_err) + np.isinf(surv_err))] = 0.0
            survival = np.array([surv, surv_err])
        else:
            survival = None

        return DataSet(
            params,
            reps,
            num_images,
            rois,
            threshold,
            img_avg,
            roi_locs,
            roi_imgs.mean(axis=1),
            roi_totals,
            mpc,
            fat,
            survival,
        )

    def iter_indiv(
        self,
        shot_num: int=0,
    ) -> tuple[
        list[str],
        Iterator[tuple[
            list[int],
            list[float],
            np.ndarray,
            np.ndarray,
            np.ndarray,
            np.ndarray,
        ]]
    ]:
        """
        Generate objects to facilitate iteration over the "populational"
        quantities from the data set:
            - sets of ROI photon totals
            - averaged complete images
            - averaged optimal ROIs
            - optimal ROI locations

        Parameters
        ----------
        shot_num : int >= 0 (optional)
            Iterate over the shot_num-th shot.

        Returns
        -------
        iterlabels : list[str]
            Labels for all parameters being iterated over.
        iterator : Iterator[tuple[Q, V, I, L, R, T]]
            Iterator over:
                Q : list[int]
                    Parameter value indices.
                V : list[float]
                    Parameter values.
                I : numpy.ndarray[ndim=2, dtype=numpy.float64]
                    Averaged complete images: Axes are image i, j coordinates.
                L : numpy.ndarray[ndim=4, dtype=numpy.float64]
                    Per-ROI sets of optimal ROI locations: Axis 0 indexes the
                    ROI; axis 1 the rep; axes 2 and 3 form the array
                        array([[ x, y ],
                               [ w, h ]])
                R : numpy.ndarray[ndim=3, dtype=numpy.float64]
                    Per-ROI averaged sub-images formed from slicing complete
                    images down to their optimal ROIs and averaging over reps:
                    Axis 0 indexes the ROI; axes 1 and 2 the image coordinate.
                T : numpy.ndarray[ndim=3, dtype=numpy.float64]
                    Per-ROI sets of photon totals: Axis 0 indexes
                    quantity/error; axis 1 the ROI; axis 2 the rep.
        """
        iterlabels = list(self.params.keys())
        param_idx = product(*[range(len(X)) for X in self.params.values()])
        p = abs(int(shot_num))
        iterator = (
            (
                list(Q),
                [self.params[i][q] for i, q in enumerate(Q)],
                self.img_avg[( *Q, p, S[:], S[:] )],
                self.roi_optim_locs[( S[:], S[:], *Q, p, S[:], S[:] )],
                self.roi_img_avg[( S[:], *Q, p, S[:], S[:] )],
                self.roi_totals[( S[:], S[:], S[:], *Q, p )],
            )
            for Q in param_idx
        )
        return iterlabels, iterator

    def iter_agg(
        self,
        axes: Sequence[int],
        shot_num: int=0,
    ) -> tuple[
        list[str],
        list[tuple[str, np.ndarray]],
        Iterator[tuple[
            list[int],
            list[float],
            np.ndarray,
            np.ndarray,
        ]]
    ]:
        """
        Generate objects to facilitate iteration over "aggregated" quantities
        in the data set, leaving some number of axes unsliced for plotting.

        Parameters
        ----------
        axes : Sequence[int]
            M axis numbers to be left unsliced in the iterand.
        shot_num : int >= 0 (optional)
            Iterate over post shots, otherwise pre shots.

        Returns
        -------
        plotvals : list[tuple[str, numpy.ndarray[ndim=1, dtype=numpy.float64]]]
            Labels and value arrays for all M parameters left unsliced.
        iterlabels : list[str]
            Labels for all parameters being iterated over.
        iterator : Iterator[tuple[Q, V, T, P, F, S]]
            Iterator over:
                Q : list[int]
                    Parameter value indices.
                V : list[float]
                    Parameter values.
                T : numpy.ndarray[ndim=3+M, dtype=numpy.float64]
                    Per-ROI sets of photon totals: Axis 0 indexes
                    quantity/error; axis 1 the ROI; axis 2 the rep.
                P : numpy.ndarray[ndim=2+M, dtype=numpy.float64]
                    Mean photon counts for each ROI. Axis 0 indexes
                    quantity/error; axis 1 the ROI.
                F : numpy.ndarray[ndim=2+M, dtype=numpy.float64]
                    Fraction above threshold for each ROI. Axis 0 indexes
                    quantity/error; axis 1 the ROI.
                S : numpy.ndarray[ndim=2+M, dtype=numpy.float64]
                    Survival fraction for each ROI if the data has pre/post
                    structure. Axis 0 indexes quantity/error; axis 1 the ROI.
                    If the data does not have pre/post structure, this value is
                    always `None`.
        """
        iterlabels = [
            label
            for i, label in self.params.keys_indexed()
            if i not in axes
        ]
        plotvals = [
            (label, values)
            for i, label, values in self.params.items_indexed()
            if i in axes
        ]
        param_idx = product(*[
            [S[:]] if i in axes else range(len(X))
            for i, X in self.params.values_indexed()
        ])
        p = abs(int(shot_num))
        iterator = (
            (
                [q for i, q in enumerate(Q) if i not in axes],
                [self.params[i][q] for i, q in enumerate(Q) if i not in axes],
                self.roi_totals[( S[:], S[:], S[:], *Q, p )],
                self.mpc[( S[:], S[:], *Q, p )],
                self.fat[( S[:], S[:], *Q, p )],
                self.survival[( S[:], S[:], *Q )]
                    if self.num_images > 1 else None,
            )
            for Q in param_idx
        )
        return plotvals, iterlabels, iterator

