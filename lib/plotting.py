
from __future__ import annotations
import numpy as np
import matplotlib
import matplotlib.pyplot as pp
pp.rcParams["axes.linewidth"] = 2.0
pp.rcParams["axes.titlesize"] = "medium"
pp.rcParams["figure.figsize"] = [3.225 * 5, 2.75 * 5]
pp.rcParams["font.size"] = 20.0
pp.rcParams["lines.linewidth"] = 2.0
pp.rcParams["lines.markeredgewidth"] = 2.0
pp.rcParams["lines.markersize"] = 5.0
import matplotlib.image as mplimg
import matplotlib.colors as mplcolors
from matplotlib import cm
hist_colors = mplcolors.LinearSegmentedColormap.from_list(
    "vibrant",
    [
        (0.000, "#012d5e"),
        (0.125, "#0039a7"),
        (0.250, "#1647cf"),
        (0.375, "#6646ff"),
        (0.500, "#bc27ff"),
        (0.600, "#dc47af"),
        (0.800, "#f57548"),
        (0.900, "#f19e00"),
        (0.950, "#fbb800"),
        (1.000, "#fec800"),
    ],
)
from typing import Sequence
from .image import ROI, S

def render_image(
    img: np.ndarray,
    cmap="gray",
    vmin: float=0.0,
    vmax: float=None,
    clabel: str="",
    title: str="",
    draw_guides: bool=True,
    rois: Sequence[ROI]=None,
    roi_optim_locs: np.ndarray=None,
    figax: tuple[pp.Figure, pp.Axes]=None,
) -> tuple[pp.Figure, pp.Axes]:
    """
    Render a single averaged image of the entire capture area.
    img : [img_i, img_j]
    """
    rois = list() if rois is None else rois
    roi_optim_locs = np.array([]) if roi_optim_locs is None else roi_optim_locs
    fig, ax = pp.subplots(figsize=[20.0, 20.0]) \
        if figax is None else figax
    im = ax.imshow(
        img,
        cmap=cmap,
        vmin=vmin,
        vmax=img.max() if vmax is None else vmax
    )
    cbar = pp.colorbar(im, ax=ax)
    if draw_guides:
        for k, (roi, locs) in enumerate(zip(rois, roi_optim_locs)):
            color = "r" if len(rois) == 1 else hist_colors(k / (len(rois) - 1))
            ax.plot(
                *roi.padded_box_coords(),
                color=color,
                linestyle="--",
                linewidth=1.5,
            )
            for box in roi.box_coords_multi(locs):
                ax.plot(
                    *box,
                    color=color,
                    linestyle="-",
                    linewidth=1.5,
                    alpha=0.1,
                )
    ax.set_title(title)
    cbar.set_label(clabel)
    return fig, ax

def render_subimages(
    imgs: Sequence[np.ndarray],
    cmap="gray",
    vmin: float=0.0,
    vmax: float=None,
    clabel: str="",
    title: str="",
    draw_guides: bool=True,
    figax: tuple[pp.Figure, Sequence[pp.Axes]]=None,
) -> tuple[pp.Figure, Sequence[pp.Axes]]:
    """
    Render several ROI sub-images (optima averaged together) in a grid of
    plotting areas.
    imgs : [roi, roi_i, roi_j]
    """
    ncols = int(np.ceil(np.sqrt(len(imgs))))
    nrows = int(np.ceil(np.sqrt(len(imgs))))
    nrows -= (nrows * ncols - len(data)) // ncols
    fig, axs = pp.subplots(
        nrows=nrows, ncols=ncols,
        figsize=[2.5 * 3.225 * ncols, 2.5 * 2.25 * nrows],
    ) \
        if figax is None else figax
    if isinstance(axs, np.ndarray):
        axs = axs.flatten()
    else:
        axs = np.array([axs])
    for k, (img, ax) in enumerate(zip(imgs, axs)):
        im = ax.imshow(
            img,
            cmap=cmap,
            vmin=vmin,
            vmax=img.max() if vmax is None else vmax
        )
        cbar = pp.colorbar(im, ax=ax)
        if draw_guides:
            color = "r" if len(imgs) == 1 else hist_colors(k / (len(imgs) - 1))
            ax.text(
                0, 0, f"{k}",
                color=color, fontsize="x-small",
                ha="left", va="top",
            )
    fig.suptitle(title + f"\ncolorbar: {clabel}", fontsize=8 * ncols)
    return fig, axs

def histogram(
    data: Sequence[np.ndarray],
    mean: float,
    mean_err: float,
    threshold: float,
    bins: np.ndarray=None,
    density: bool=True,
    xlabel: str="",
    ylabel: str="",
    title: str="",
    figax: tuple[pp.Figure, pp.Axes]=None,
) -> tuple[pp.Figure, pp.Axes]:
    """
    Generate a histogram of data for several ROIs, stacking them in different
    colors.
    data : [roi, rep]
    """
    if (
        (isinstance(data, np.ndarray) and len(data.shape) == 1)
        or len(data) == 1
    ):
        colors = ["C0"]
    else:
        colors = [hist_colors(k / (len(data) - 1)) for k in range(len(data))]
    fig, ax = pp.subplots() \
        if figax is None else figax
    ax.hist(
        data.T,
        bins,
        density=density,
        stacked=True,
        color=colors,
    )
    ylim = ax.get_ylim()
    ax.fill_betweenx(
        ylim,
        2 * [mean - mean_err],
        2 * [mean + mean_err],
        color="k",
        alpha=0.2,
    )
    ax.axvline(mean, color="k", linestyle="-", linewidth=3)
    ax.text(
        mean, 0.95 * ylim[1],
        f"${mean:.3f} \\pm {mean_err:.3f}$",
        ha="center", va="top",
        color="k",
        bbox=dict(
            facecolor="w",
            linewidth=0.0,
            alpha=0.85,
            pad=0.2,
        ),
    )
    ax.axvline(threshold, color="r", linestyle="-", linewidth=2.5)
    ax.set_ylim(*ylim)
    ax.minorticks_on()
    ax.grid(True, "major")
    ax.grid(True, "minor", linestyle=":")
    ax.tick_params(which="both", direction="in")
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    ax.set_title(title, fontsize=16)
    return fig, ax

def histogram_prepost(
    data: Sequence[np.ndarray],
    mean: tuple[float, float],
    mean_err: tuple[float, float],
    threshold: float,
    bins: np.ndarray=None,
    density: bool=True,
    xlabel: str="",
    ylabel: str="",
    title: str="",
) -> tuple[pp.Figure, list[pp.Axes, pp.Axes, pp.Axes]]:
    """
    Generate a scatter plot of pre/post photon count data, with histograms
    for both shots added on the x- and y-axes.
    data : [roi, rep, pre/post]
    """
    if (
        (isinstance(data, np.ndarray) and len(data.shape) == 1)
        or len(data) == 1
    ):
        colors = ["C0"]
    else:
        colors = [hist_colors(k / (len(data) - 1)) for k in range(len(data))]
    fig = pp.figure(figsize=[3.225 * 5, 3.200 * 5])
    gs = fig.add_gridspec(
        2, 2,
        width_ratios=[1, 2], height_ratios=[2, 1],
        wspace=0.05, hspace=0.05
    )
    ax_hist_pre = fig.add_subplot(gs[1, 1])
    ax_hist_post = fig.add_subplot(gs[0, 0])
    ax_scatter = fig.add_subplot(gs[0, 1], sharex=ax_hist_pre, sharey=ax_hist_post)

    ax_hist_pre.hist(
        data[:, :, 0].T,
        bins,
        density=density,
        stacked=True,
        color=colors,
    )
    ylim = ax_hist_pre.get_ylim()
    ax_hist_pre.fill_betweenx(
        ylim,
        2 * [mean[0] - mean_err[0]],
        2 * [mean[0] + mean_err[0]],
        color="k",
        alpha=0.2,
    )
    ax_hist_pre.axvline(mean[0], color="k", linestyle="-", linewidth=3)
    ax_hist_pre.text(
        mean[0], 0.95 * ylim[1],
        f"${mean[0]:.3f} \\pm {mean_err[0]:.3f}$",
        ha="center", va="top",
        color="k",
        bbox=dict(
            facecolor="w",
            linewidth=0.0,
            alpha=0.85,
            pad=0.2,
        ),
    )
    ax_hist_pre.axvline(threshold, color="r", linestyle="-", linewidth=2.5)
    ax_hist_pre.set_ylim(*ylim)
    ax_hist_pre.grid(True, "major")
    ax_hist_pre.grid(True, "minor", linestyle=":")
    ax_hist_pre.tick_params(which="both", direction="in")
    ax_hist_pre.set_xlabel(xlabel + " (pre)")
    ax_hist_pre.set_ylabel(ylabel)

    ax_hist_post.hist(
        data[:, :, 1].T,
        bins,
        density=density,
        stacked=True,
        color=colors,
        orientation="horizontal",
    )
    xlim = ax_hist_post.get_xlim()
    ax_hist_post.fill_between(
        xlim,
        2 * [mean[1] - mean_err[1]],
        2 * [mean[1] + mean_err[1]],
        color="k",
        alpha=0.2,
    )
    ax_hist_post.axhline(mean[1], color="k", linestyle="-", linewidth=3)
    ax_hist_post.text(
        0.95 * xlim[1], mean[1],
        f"${mean[1]:.3f} \\pm {mean_err[1]:.3f}$",
        ha="right", va="center", rotation=270.0,
        color="k",
        bbox=dict(
            facecolor="w",
            linewidth=0.0,
            alpha=0.85,
            pad=0.2,
        ),
    )
    ax_hist_post.axhline(threshold, color="r", linestyle="-", linewidth=2.5)
    ax_hist_post.set_xlim(*xlim)
    ax_hist_post.grid(True, "major")
    ax_hist_post.grid(True, "minor", linestyle=":")
    ax_hist_post.tick_params(which="both", direction="in")
    ax_hist_post.set_xlabel(ylabel)
    ax_hist_post.set_ylabel(xlabel + " (post)")

    for k, c in enumerate(colors):
        ax_scatter.plot(
            data[k, :, 0], data[k, :, 1],
            marker="o", linestyle="", color=c, alpha=0.1,
        )
    xlim = ax_scatter.get_xlim()
    ylim = ax_scatter.get_ylim()
    ax_scatter.fill_betweenx(
        ylim,
        2 * [mean[0] - mean_err[0]],
        2 * [mean[0] + mean_err[0]],
        color="k",
        alpha=0.2,
    )
    ax_scatter.axvline(mean[0], color="k", linestyle="-", linewidth=3)
    ax_scatter.fill_between(
        xlim,
        2 * [mean[1] - mean_err[1]],
        2 * [mean[1] + mean_err[1]],
        color="k",
        alpha=0.2,
    )
    ax_scatter.axhline(mean[1], color="k", linestyle="-", linewidth=3)
    ax_scatter.axvline(threshold, color="r", linestyle="-", linewidth=2.5)
    ax_scatter.axhline(threshold, color="r", linestyle="-", linewidth=2.5)
    ax_scatter.set_xlim(*xlim)
    ax_scatter.set_ylim(*ylim)
    ax_scatter.minorticks_on()
    ax_scatter.grid(True, "major")
    ax_scatter.grid(True, "minor", linestyle=":")
    ax_scatter.tick_params(
        which="both",
        direction="in",
        labelbottom=False,
        labelleft=False
    )
    fig.suptitle(title, fontsize=16)
    return fig, (ax_hist_pre, ax_hist_post, ax_scatter)

def subimages_histogram(
    data: Sequence[np.ndarray],
    means: Sequence[float],
    means_err: Sequence[float],
    threshold: float,
    bins: np.ndarray=None,
    density: bool=True,
    xlabel: str="",
    ylabel: str="",
    title: str="",
    figax: tuple[pp.Figure, Sequence[pp.Axes]]=None,
) -> tuple[pp.Figure, Sequence[pp.Axes]]:
    """
    Generate histograms for several ROIs, each on its own plot in a grid.
    data : [roi, rep]
    """
    ncols = int(np.ceil(np.sqrt(len(data))))
    nrows = int(np.ceil(np.sqrt(len(data))))
    nrows -= (nrows * ncols - len(data)) // ncols
    fig, axs = pp.subplots(
        nrows=nrows, ncols=ncols,
        figsize=[2.5 * 3.25 * ncols, 2.5 * 2.5 * nrows]
    ) \
        if figax is None else figax
    if isinstance(axs, np.ndarray):
        axs = axs.flatten()
    else:
        axs = np.array([axs])
    if (
        (isinstance(data, np.ndarray) and len(data.shape) == 1)
        or len(data) == 1
    ):
        colors = ["C0"]
    else:
        colors = [hist_colors(k / (len(data) - 1)) for k in range(len(data))]
    for c, D, m, e, ax in zip(colors, data, means, means_err, axs):
        ax.hist(
            D,
            bins,
            density=density,
            color=c,
        )
        ylim = ax.get_ylim()
        ax.fill_betweenx(
            ylim,
            2 * [m - e],
            2 * [m + e],
            color="k",
            alpha=0.2,
        )
        ax.axvline(m, color="k", linestyle="-", linewidth=2)
        ax.text(
            m, 0.95 * ylim[1],
            f"${m:.3f} \\pm {e:.3f}$",
            ha="center", va="top",
            color="k",
            bbox=dict(
                facecolor="w",
                linewidth=0.0,
                alpha=0.85,
                pad=0.2,
            ),
        )
        ax.grid()
    fig.suptitle(title + f"\n$x$: {xlabel}; $y$: {ylabel}", fontsize=8 * ncols)
    return fig, axs

def gen_extent(
    x: np.ndarray,
    y: np.ndarray,
) -> tuple[float, float, float, float]:
    xdiff = np.diff(x)
    dx = (
        xdiff[ 0] if len(x) > 1 else 1.0,
        xdiff[-1] if len(x) > 1 else 1.0,
    )
    ydiff = np.diff(y)
    dy = (
        ydiff[ 0] if len(y) > 1 else 1.0,
        ydiff[-1] if len(y) > 1 else 1.0,
    )
    extent = (
        x[ 0] - dx[0] / 2.0, x[-1] + dx[1] / 2.0,
        y[ 0] - dy[0] / 2.0, y[-1] + dy[1] / 2.0,
    )
    return extent

def colorplot(
    x: np.ndarray,
    y: np.ndarray,
    Z: np.ndarray,
    ERR: np.ndarray=None,
    cmap="jet",
    xlabel: str="",
    ylabel: str="",
    clabel: str="",
    title: str="",
    figax: tuple[pp.Figure, pp.Axes]=None,
) -> tuple[
    tuple[pp.Figure, pp.Axes],
    tuple[pp.Figure, pp.Axes],
]:
    """
    Generate a 2D color map for a quantity with two independent variables, with
    standard error for the quantity on a separate, duplicate map.
    x : [val_j]
    y : [val_i]
    Z : [val_i, val_j]
    ERR : [val_i, val_j]
    """
    ERR = np.zeros(Z.shape) if ERR is None else ERR
    # sort_idx_j = np.argsort(x)
    # sort_idx_i = np.argsort(y)
    # x = np.sort(x)
    # y = np.sort(y)
    # Z = S.sort_idx_nd(Z, tuple(), sort_idx_i, sort_idx_j)
    # ERR = S.sort_idx_nd(ERR, tuple(), sort_idx_i, sort_idx_j)

    extent = gen_extent(x, y)
    fig, ax = pp.subplots() \
        if figax is None else figax
    im = mplimg.NonUniformImage(
        ax,
        interpolation="nearest",
        extent=extent,
        cmap=cmap,
    )
    im.set_data(x, y, Z)
    ax.add_image(im)
    cbar = fig.colorbar(im, ax=ax)
    ax.set_xlim(extent[0], extent[1])
    ax.set_ylim(extent[2], extent[3])
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    cbar.set_label(clabel)
    ax.set_title(title)

    fig_err, ax_err = pp.subplots()
    im_err = mplimg.NonUniformImage(
        ax_err,
        interpolation="nearest",
        extent=extent,
        cmap=cmap,
    )
    im_err.set_data(x, y, ERR)
    ax_err.add_image(im_err)
    cbar_err = fig_err.colorbar(im_err, ax=ax_err)
    ax_err.set_xlim(extent[0], extent[1])
    ax_err.set_ylim(extent[2], extent[3])
    ax_err.set_xlabel(xlabel)
    ax_err.set_ylabel(ylabel)
    cbar_err.set_label(clabel + " (std. err.)")
    ax_err.set_title(title)

    return (fig, ax), (fig_err, ax_err)

def multilineplot(
    x: np.ndarray,
    y: np.ndarray,
    Z: np.ndarray,
    ERR: np.ndarray=None,
    POP: np.ndarray=None,
    cmap="jet",
    xlabel: str="",
    ylabel: str="",
    zlabel: str="",
    title: str="",
    versus_axis: int=1,
    normalize: bool=False,
    figax: tuple[pp.Figure, pp.Axes]=None,
) -> tuple[pp.Figure, pp.Axes]:
    """
    Similar to `colorplot`, but plot using multiple lines plotted against one of
    the two independent variables. If `POP` is provided, add violin plots behind
    each data point.
    x : [val_j]
    y : [val_i]
    Z : [val_i, val_j]
    ERR : [val_i, val_j]
    POP : [rep, val_i, val_j]
    """
    ERR = np.zeros(Z.shape) if ERR is None else ERR
    has_pop = POP is not None
    # sort_idx_j = np.argsort(x)
    # sort_idx_i = np.argsort(y)
    # x = np.sort(x)
    # y = np.sort(y)
    # Z = S.sort_idx_nd(Z, tuple(), sort_idx_i, sort_idx_j)
    # ERR = S.sort_idx_nd(ERR, tuple(), sort_idx_i, sort_idx_j)
    if has_pop:
        POP = S.sort_idx_nd(
            POP, tuple(), np.arange(POP.shape[0]), sort_idx_i, sort_idx_j)

    if normalize:
        if versus_axis:
            maxes = Z.max(axis=1)
            Z = np.array([Z[k, :] / M for k, M in enumerate(maxes)])
            ERR = np.array([ERR[k, :] / M for k, M in enumerate(maxes)])
            if has_pop:
                POP = np.array([
                    np.array([pop[k, :] / M for k, M in enumerate(maxes)])
                    for pop in POP
                ])
        else:
            maxes = Z.max(axis=0)
            Z = np.array([Z[:, k] / M for k, M in enumerate(maxes)]).T
            ERR = np.array([ERR[:, k] / M for k, M in enumerate(maxes)]).T
            if has_pop:
                POP = np.array([
                    np.array([pop[k, :] / M for k, M in enumerate(maxes)]).T
                    for pop in POP
                ])

    cmap_f = cm.get_cmap(cmap) if isinstance(cmap, str) else cmap
    fig, ax = pp.subplots() \
        if figax is None else figax
    if versus_axis:
        ax.plot(
            [], [],
            marker="", linestyle="", color="w",
            label=ylabel
        )
        for k, yval in enumerate(y):
            color = cmap_f(k / (len(y) - 1)) if len(y) > 1 else "C0"
            if has_pop:
                v = ax.violinplot(
                    POP[:, k, :], x,
                    showmeans=False, showextrema=False, showmedians=False,
                    widths=1.0
                )
                for vi in v["bodies"]:
                    vi.set_facecolor(cmap_f(k / (len(y) - 1)))
                    vi.set_linewidth(0.0)
            ax.errorbar(
                x, Z[k, :], ERR[k, :],
                marker="o", linestyle="-", color=color,
                label=f"{yval:.5f}",
            )
            if len(x) == 1:
                ax.text(
                    x[0], Z[k, 0], f"  ${Z[k, 0]:.5f} \\pm {ERR[k, 0]:.5f}$",
                    color="k", fontsize="x-small",
                    ha="left", va="center",
                )
    else:
        ax.plot(
            [], [],
            marker="", linestyle="", color="w",
            label=xlabel
        )
        for k, xval in enumerate(x):
            color = cmap_f(k / (len(x) - 1)) if len(x) > 1 else "C0"
            if has_pop:
                v = ax.violinplot(
                    POP[:, :, k], y,
                    showmeans=False, showextrema=False, showmedians=True,
                    widths=1.0
                )
                for vi in v["bodies"]:
                    vi.set_facecolor(cmap_f(k / (len(x) - 1)))
                    vi.set_linewidth(0.0)
            ax.errorbar(
                y, Z[:, k], ERR[:, k],
                marker="o", linestyle="-", color=color,
                label=f"{xval:.5f}"
            )
            if len(y) == 1:
                ax.text(
                    y[0], Z[0, k], f"  ${Z[0, k]:.5f} \\pm {ERR[0, k]:.5f}$",
                    color="k", fontsize="x-small",
                    ha="left", va="center",
                )
    ax.minorticks_on()
    ax.grid(True, "major")
    ax.grid(True, "minor", linestyle=":")
    ax.tick_params(which="both", direction="in")
    ax.set_xlabel(xlabel if versus_axis else ylabel)
    ax.set_ylabel(("Norm. " + zlabel) if normalize else zlabel)
    ax.set_title(title, fontsize=14.0)
    ax.legend(fontsize=14.0)
    return fig, ax

def lineplot(
    x: np.ndarray,
    y: Sequence[np.ndarray],
    err: Sequence[np.ndarray]=None,
    pop: Sequence[np.ndarray]=None,
    xlabel: str="",
    ylabel: str="",
    title: str="",
    normalize: bool=False,
    indep_rois: bool=True,
    figax: tuple[pp.Figure, pp.Axes]=None,
) -> tuple[pp.Figure, pp.Axes]:
    """
    Generate a line plot for a single dependent variable versus a single
    independent variable on several ROIs. If `pop` is provided, add violin plots
    behind each data point.
    x : [val_k]
    y : [roi, val_k]
    err : [roi, val_k]
    pop : [roi, rep, val_k]
    """
    if (
        (isinstance(y, np.ndarray) and len(y.shape) == 1)
        or len(y) == 1
    ):
        colors = ["C0"]
    else:
        colors = [hist_colors(k / (len(y) - 1)) for k in range(len(y))]

    err = np.zeros(y.shape) if err is None else err
    has_pop = pop is not None
    if normalize:
        maxes = y.max(axis=-1) * np.ones(y.shape[0])
        y = np.array([yk / M for yk, M in zip(y, maxes)])
        err = np.array([errk / M for errk, M in zip(err, maxes)])
        if has_pop:
            pop = np.array([popk / M for popk, M in zip(pop, maxes)])

    fig, ax = pp.subplots() \
        if figax is None else figax
    if indep_rois:
        for k, (yk, errk, c) in enumerate(zip(y, err, colors)):
            if has_pop:
                v = ax.violinplot(
                    pop[k, :, :], x,
                    showmeans=False, showextrema=False, showmedians=False,
                    widths=1.0
                )
                for vi in v["bodies"]:
                    vi.set_facecolor(c)
                    vi.set_linewidth(0.0)
            ax.errorbar(
                x, yk, errk,
                marker="o", linestyle="-", color=c
            )
            if len(x) == 1:
                ax.text(
                    x[0], yk[0], f"  ${yk[0]:.5f} \\pm {errk[0]:.5f}$",
                    color=c, fontsize="x-small",
                    ha="left", va="center",
                )
    if has_pop:
        v = ax.violinplot(
            pop.mean(axis=0), x,
            showmeans=False, showextrema=False, showmedians=False,
            widths=1.0
        )
        for vi in v["bodies"]:
            vi.set_facecolor("k" if len(y) > 1 else "C0")
            vi.set_linewidth(0.0)
    m = y.mean(axis=0)
    e = np.sqrt((err**2).sum(axis=0)) / err.shape[0]
    ax.errorbar(
        x, m, e,
        marker="o", linestyle="-", color="k" if len(y) > 1 else "C0"
    )
    if len(x) == 1:
        ax.text(
            x[0], m[0], f"  ${m[0]:.5f} \\pm {e[0]:.5f}$",
            color="k", fontsize="x-small",
            ha="left", va="center",
        )
    ax.minorticks_on()
    ax.grid(True, "major")
    ax.grid(True, "minor", linestyle=":")
    ax.tick_params(which="both", direction="in")
    ax.set_xlabel(xlabel)
    ax.set_ylabel(("Norm. " + ylabel) if normalize else ylabel)
    ax.set_title(title)
    return fig, ax

