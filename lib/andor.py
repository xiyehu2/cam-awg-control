import numpy as np
from pylablib.devices import Andor
from typing import Tuple, Dict, BinaryIO

def restart_lib():
    Andor.AndorSDK2.restart_lib()

class iXon888:
    """
    Simple class to handle image acquisition from an Andor iXon 888.

    Methods without explicit return types return `self`.
    """
    def __init__(self, idx: int=0):
        self.cam_idx = idx
        self.cam: Andor.AndorSDK2Camera = None

    def is_connected(self) -> bool:
        return self.cam is not None and self.cam.is_opened()

    def open(self, ini_path:str=None) -> bool:
        try:
            self.cam = Andor.AndorSDK2Camera(
                self.cam_idx,
                temperature="off",
                fan_mode="full",
                amp_mode=None,
            )
            self.cam.open()
            return True
        except:
            return False

    def connect(self, ini_path:str=None) -> bool:
        return self.open(ini_path)

    def close(self):
        if self.is_connected():
            self.set_cooler(False)
            self.cam.close()
            self.cam = None
        return self

    def disconnect(self):
        return self.close()

    def is_acquiring(self) -> bool:
        if self.is_connected():
            return self.cam.acquisition_in_progress()
        return False

    def clear_acquisition_settings(self):
        if self.is_connected():
            self.cam.clear_acquisition()
        return self

    def get_device_info(self) -> (str, str, int):
        if self.is_connected():
            return self.cam.get_device_info()
        return "", "", -1

    def get_status(self) -> str:
        if self.is_connected():
            return self.cam.get_status()
        return "camera not connected"

    def set_frame_transfer_mode(self, onoff: bool):
        if self.is_connected():
            self.cam.enable_frame_transfer_mode(onoff)
        return self

    def get_em_gain(self) -> (int, int):
        if self.is_connected():
            return self.cam.get_EMCCD_gain()
        return -1, -1

    def set_em_gain(self, gain):
        if self.is_connected():
            self.cam.set_EMCCD_gain(gain)
        return self

    def set_temperature(self, temp:int=-80, cooler_on=True):
        if self.is_connected():
            self.cam.set_temperature(temp, cooler_on)
        return self

    def get_temperature(self) -> int:
        if self.is_connected():
            return self.cam.get_temperature()
        return 0

    def set_cooler(self, on=True):
        if self.is_connected():
            self.cam.set_cooler(on)
        return self

    def is_cooler_on(self) -> bool:
        if self.is_connected():
            return self.cam.is_cooler_on()
        return False

    def set_exposure(self, exposure):
        if self.is_connected():
            self.cam.set_exposure()
        return self

    def get_exposure(self) -> int:
        if self.is_connected():
            return self.cam.get_exposure()
        return -1

    def set_acquisition_mode(self, mode:str="single"):
        if self.is_connected():
            self.cam.set_acquisition_mode(mode)
        return self

    def setup_shutter(self, mode:str="auto", ttl_mode:int=1):
        if self.is_connected():
            self.cam.setup_shutter(mode, ttl_mode)
        return self

    def set_trigger_mode(self, mode:str="ext"):
        if self.is_connected():
            self.cam.set_trigger_mode(mode)
        return self

    def setup_ext_trigger(self, level=None, invert=None, term_highZ=None):
        self.cam.setup_ext_trigger(level, invert, term_highZ)
        return self

    def set_read_mode(self, mode='image'):
        if self.is_connected():
            self.cam.set_read_mode(mode)
        return self

    def set_roi(self, roi:np.array):
        if self.is_connected():
            self.cam.set_roi(roi[0], roi[1], roi[2], roi[3])
        return self

    def get_roi(self) -> (int, int, int, int):  # tuple(hstart, hend, vstart, vend, hbin, vbin)
        if self.is_connected():
            return self.cam.get_roi()
        return -1, -1, -1, -1

    def start_acquisition(self) -> bool:
        if self.is_connected() and self.is_acquisition_setup():
            self.cam.start_aquisition()
            return True
        return False

    def stop_acquisition(self) -> bool:
        if self.is_connected() and self.is_acquiring():
            self.cam.stop_acquisition()
            return True
        return False

    def get_acquisition_params(self) -> Dict[str, str]:
        if self.is_connected():
            return self.cam.get_acquisition_parameters()
        return {}

    def get_full_info(self) -> Dict[str, str]:
        if self.is_connected():
            return self.cam.get_full_info()
        return {}

    def is_acquisition_setup(self) -> bool:
        if self.is_connected():
            return self.cam.is_acquisition_setup()
        return False

    def read_newest_image(self, peek=False):
        if self.is_acquiring():
            return self.cam.read_newest_image(peek)
        return None

    def read_oldest_image(self, peek=False):
        if self.is_acquiring():
            return self.cam.read_oldest_image(peek)
        return None

    def set_frame_format(self, fmt='try_chunks'):  # "list", "array", "chunks"
        if self.is_connected():
            self.cam.set_frame_format(fmt)
        return self

    def wait_for_image(self, since='lastread', nimages=1, timeout=600):
        if self.is_acquiring():
            return self.cam.wait_for_frame(since, nimages, timeout)
        return None

    def read_multiple_images(
        self,
        nimages:int = None,  # (first, last), first inclusive
        peek:bool = False,
        missing:str = 'skip',  # None, zero, skip
    ):
        if self.is_acquiring():
            return self.cam.read_multiple_images((0, nimages), peek, missing)
        return None

    def get_frames_status(self) -> Tuple[int, int, int, int]:
        # tuple(acquired, unread, skipped, size)
        if self.is_acquisition_setup():
            return self.cam.get_frames_status()
        return -1, -1, -1, -1

    def get_new_images_range(self) -> (int, int):
        if self.is_acquiring():
            return self.cam.get_new_images_range()
        return None

    def grab(self):
        if self.is_connected():
            img = self.cam.grab()
            return img
        return None

    def send_software_trig(self):
        if self.is_connected():
            self.cam.send_software_trigger()

    def set_amp_mode(
        self,
        channel=None,
        oamp=None,
        hsspeed=None,
        preamp=None,
    ) -> bool:
        if self.is_connected():
            self.cam.set_amp_mode(channel, oamp, hsspeed, preamp)
            return True
        return False

    def get_amp_mode(self, full=True):
        if self.is_connected():
            return self.cam.get_amp_mode(full)
        return None

    # def setup_camera(self, config_path=None) -> bool:
    #     self.connect(ini_path=config_path)
    #     if self.is_connected():
    #         print(self.get_full_info())
    #         print(self.get_acquisition_params())
    #         print("Camera is connected")
    #         return True
    #     else:
    #         print("Cam connection failed")
    #         return False

    # def start_acquisition(
    #         self,
    #         loop_params: (int,int) = (0,1),
    # ) -> (bool, BinaryIO):
    #     is_setup = False
    #     if self.is_acquisition_setup():
    #         self.acquire()
    #         is_setup = True
    #         print("Acquisition started")
    #     else:
    #         print("Acquisition not setup")
    #     return is_setup

    # def stop_acquisition(self, outfile):
    #     if self.is_connected() and self.is_acquiring():
    #         self.stop_acquisition()
    #     if outfile is not None:
    #         outfile.close()
    #         outfile = None
    #     return self
