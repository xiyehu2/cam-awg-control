import time
from pathlib import Path
import dearpygui.dearpygui as dpg
import lib.andor as andor
import numpy as np
from threading import Thread
from queue import Queue
from feedforward import feedforward
import serial
from typing import List, BinaryIO

def toggle_button(sender):
    enable_state = dpg.get_item_configuration(sender)['enabled']
    dpg.configure_item(sender, enabled=not enable_state)

def acquire_button(
    sender,
    app_data,
    user_data,
):
    outdir = Path(dpg.get_value("outdir"))
    outfile = dpg.get_value("outfile")
    counter = dpg.get_value("counter")
    write_file = None
    save = dpg.get_value("save_data")
    emccd, fdfwd_idx = user_data
    fdfwd_queue = Queue()
    img_queue = Queue
    if emccd.start_acquisition():
        if save:
            if not outdir.is_dir():
                print(f":: mkdir -p {outdir}")
                outdir.mkdir(parents=True)
            while outdir.joinpath(f"{outfile}_{counter:03.0f}").is_file():
                counter += 1
            write_file = open(outdir.joinpath(f"{outfile}_{counter:03.0f}"), 'xb')
            dpg.set_value("write_file", write_file)

        Thread(
            target=img_acquisition_loop,
            args=(emccd, img_queue, fdfwd_idx, fdfwd_queue),
        ).start()
        Thread(
            target=img_plot_and_save_loop,
            args=(img_queue, write_file),
        ).start()
        if dpg.get_value("feedforward"):
            Thread(
                target=feedforward,
                args=(fdfwd_queue, fdfwd_idx, dpg.get_value("image_roi"))
            ).start()
        toggle_button("acquire_button")
        toggle_button("stop_button")
        dpg.set_value("is_acquiring", True)
        print("Acquisition started.")
    else:
        print("Acquisition not set up.")

def stop_button(sender, app_data, user_data: andor.iXon888):
    if user_data.stop_acquisition():
        dpg.set_value("write_file", None)
        dpg.set_value("is_acquiring", False)
        print("Acquisition stopped")
        toggle_button("stop_button")

def set_exposure_time(sender, app_data, user_data: andor.iXon888):
    if user_data.is_connected():
        user_data.set_exposure(app_data)
        dpg.set_value("exposure_time", max(app_data, 0.0))
        print(f"set camera exposure to {max(app_data, 0.0)}")


def set_em_gain(sender, app_data, user_data: andor.iXon888):
    if user_data.is_connected():
       user_data.set_em_gain(app_data)
       dpg.set_value("em_gain", max(app_data, 2))
       print(f"set camera EM gain to {max(app_data, 2)}")


def set_roi(sender, app_data, user_data: andor.iXon888):
    if user_data.is_connected():
        l = max(0, min(1023, dpg.get_value("roi_l")))
        r = max(0, min(1023, dpg.get_value("roi_r")))
        t = max(0, min(1023, dpg.get_value("roi_t")))
        b = max(0, min(1023, dpg.get_value("roi_b")))
        print(f"set camera ROI to {l=} {r=} {t=} {b=}")
        dpg.set_value("roi_l", l)
        dpg.set_value("roi_r", r)
        dpg.set_value("roi_t", t)
        dpg.set_value("roi_b", b)
        user_data.set_roi([l,r,t,b])

def set_cooling(sender, app_data, user_data: andor.iXon888):
    dpg.set_value("cooling", app_data)
    if user_data is not None and user_data.is_connected():
        user_data.set_temperature(
            dpg.get_value("temperature"),
            app_data,
        )
        print(f"set cooling={app_data}")

def set_temperature(sender, app_data, user_data: andor.iXon888):
    temp = max(-100.0, min(20.0, app_data))
    dpg.set_value("temperature", temp)
    if user_data.is_connected():
        user_data.set_temperature(
            temp,
            dpg.get_value("cooling"),
        )
        print(f"set cooling to {temp}")

def set_cbar_min(sender, data):
    dpg.set_value("cbar_min", data)
    _rescale_cbar()
    # print(f"set color bar min to {data}")

def set_cbar_max(sender, data):
    dpg.set_value("cbar_max", data)
    _rescale_cbar()
    # print(f"set color bar max to {data}")

def set_color_scale(sender, app_data, user_data):
    dpg.bind_colormap("image_plot", user_data[app_data])
    dpg.bind_colormap("cbar", user_data[app_data])
    print(f"set color scale to {app_data}")

def _rescale_cbar():
    cbar_min = dpg.get_value("cbar_min")
    cbar_max = dpg.get_value("cbar_max")
    dpg.configure_item(
        "cbar",
        min_scale=cbar_min,
        max_scale=cbar_max,
    )
    dpg.configure_item(
        "image",
        scale_min=cbar_min,
        scale_max=cbar_max,
    )

def autoscale_cbar(sender, data):
    dpg.set_value("cbar_min", dpg.get_value("image_min"))
    dpg.set_value("cbar_max", dpg.get_value("image_max"))
    _rescale_cbar()
    print("autoscale color bar")

def set_counter(sender, data):
    dpg.set_value("counter", data)

def set_outdir(sender, data):
    dpg.set_value("outdir", data["file_path_name"])

def outdir_selector(sender, data):
    dpg.add_file_dialog(
        label="Select output directory",
        callback=set_outdir,
        directory_selector=True,
        cancel_callback=False,
        width=650,
        height=400,
    )

def set_outfile(sender, data):
    P = Path(data["file_path_name"]).absolute()
    dpg.set_value("outfile", P.name)
    dpg.set_value("outdir", str(P.parent))
    check_outfile_ext(None, P.name)

def outfile_selector(sender, data):
    dpg.add_file_dialog(
        label="Choose output file",
        callback=set_outfile,
        cancel_callback=False,
        width=650,
        height=400,
    )

def check_outfile_ext(sender, data):
    if not data.endswith(".npy"):
        dpg.set_value("outfile", data + ".npy")

def set_image_loop(sender, data):
    dpg.set_value("shots_per_loop", data)
    print(f"set number of shots per loop to {data}")

def set_fdfwd_idx(sender, app_data, user_data: List):
    user_data.clear()
    for i in app_data.split(','):
        idx = int(i)
        if idx < dpg.get_value("shots_per_loop"):
            user_data.append(idx)
    print(f"set feedforward image idx to {user_data}")
    return

def enable_feedforward(sender, app_data:bool, user_data:{str: int}):
    dpg.set_value("feedforward", app_data)
    dpg.configure_item("fdfwd_idx_group", show=app_data)
    dpg.configure_item("image_roi_group", show=app_data)
    print(f"set feedforward={app_data}")

def connect_button(sender, app_data, user_data: andor.iXon888):
    if user_data.connect("config/test.cfg"):
        print("Camera connected.")
        for key, value in user_data.get_full_info().items():
            print(key, value)
        # print(user_data.get_full_info())
        toggle_button(sender)
        toggle_button("disconnect_button")
    else:
        print("Camera connection failed.")

def disconnect_button(sender, app_data, user_data: andor.iXon888):
    if user_data is not None:
        user_data.disconnect()
        if not user_data.is_connected():
            toggle_button(sender)
            toggle_button("connect_button")

def image_roi_input(sender, app_data, user_data):
    dpg.set_value("image_roi", app_data)
    print("changing image ROI to [x,y,w,h]:", dpg.get_value("image_roi"))

def img_acquisition_loop(
    emccd: andor.iXon888,
    img_queue: Queue,
    fdfwd_idx: List,
    fdfwd_queue: Queue,
):
    current_idx = 0
    shots_per_loop = dpg.get_value("shots_per_loop")
    is_fdfwd = dpg.get_value("feedforward")
    while dpg.get_value("is_acquiring"):
        emccd.wait_for_image(timeout=None)
        img = emccd.read_oldest_image()
        current_idx = (current_idx + 1) % shots_per_loop
        if is_fdfwd and (current_idx in fdfwd_idx):
            fdfwd_queue.put(*img, block=False)
        img_queue.put(*img, block=False)
    return

def img_plot_and_save_loop(
    img_queue: Queue,
    write_file: BinaryIO,
):
    is_save = write_file is not None
    while dpg.get_value("is_acquiring"):
        while not img_queue.empty():
            img = img_queue.get()
            row, col = img.shape
            if is_save:
                np.save(write_file, img)
            dpg.set_value("image_min", np.min(img))
            dpg.set_value("image_max", np.max(img))
            dpg.configure_item(
                "image",
                x=img,
                rows=row,
                cols=col,
                bounds_min=(dpg.get_value("roi_l") - 0.5, dpg.get_value("roi_b") - 0.5),
                bounds_max=(dpg.get_value("roi_r") + 0.5, dpg.get_value("roi_t") + 0.5),
            )
            dpg.set_axis_limits_auto("xaxis")
            dpg.set_axis_limits_auto("yaxis")
    if is_save:
        write_file.close()
    return

def grab_button(sender, app_data, user_data: andor.iXon888):
    if user_data is not None:
        img = user_data.grab()
        print(img)
        # np.save("lib/gui/test_outputs/test_img.npy", img)
    return

def soft_trig_button(sender, app_data, user_data: andor.iXon888):
    if user_data is not None:
        user_data.send_software_trig()

def _load_test_data():
    test_data = np.array(
        [[497, 495, 503, 502, 503, 505, 529, 535, 503, 563, 497, 516, 552,
          499, 498, 501, 502, 496, 499, 502, 501],
         [523, 502, 501, 503, 501, 500, 498, 570, 501, 503, 500, 514, 504,
          511, 503, 501, 500, 503, 498, 501, 509],
         [507, 496, 502, 508, 497, 498, 590, 563, 541, 502, 654, 501, 506,
          503, 504, 499, 501, 515, 504, 502, 502],
         [515, 496, 505, 504, 522, 500, 495, 521, 595, 579, 502, 498, 519,
          502, 495, 503, 500, 499, 503, 509, 501],
         [499, 505, 496, 500, 493, 499, 505, 554, 746, 565, 655, 725, 563,
          591, 498, 496, 500, 505, 493, 503, 499],
         [504, 512, 495, 503, 502, 505, 507, 596, 564, 785, 783, 756, 562,
          505, 536, 617, 506, 528, 502, 499, 499],
         [502, 523, 499, 509, 514, 575, 592, 522, 572, 753, 626, 576, 554,
          524, 537, 499, 502, 536, 504, 502, 509],
         [497, 504, 499, 584, 503, 512, 497, 562, 540, 558, 504, 544, 537,
          521, 496, 501, 500, 567, 504, 513, 503],
         [504, 504, 501, 501, 500, 507, 522, 498, 597, 502, 497, 499, 498,
          498, 496, 499, 500, 504, 498, 500, 499],
         [500, 495, 498, 500, 495, 499, 496, 501, 506, 507, 498, 495, 530,
          499, 508, 501, 559, 498, 502, 526, 503],
         [574, 497, 503, 505, 500, 498, 504, 500, 507, 536, 503, 506, 553,
          504, 521, 504, 577, 504, 499, 501, 504]],
        dtype=int
    )
    return test_data

def test_button(sender, app_data, user_data):
    # arduino = serial.Serial(port="COM3", baudrate=9600, timeout=1)
    # time.sleep(5)
    # text = dpg.get_value("test_str")
    # print(text, type(text), text[1:].encode('utf-8'), bytes("0001\n", 'utf-8'))
    # arduino.write(bytes(text, 'utf-8'))
    # print(arduino.readline())
    # print(arduino.readline())
    # print(arduino.readline())
    # print(arduino.readline())
    # arduino.close()

    img = np.random.randint(500, 600, 11*21, dtype=np.int).reshape(11,21)
    row, col = img.shape
    dpg.set_value("image_min", np.min(img))
    dpg.set_value("image_max", np.max(img))
    dpg.configure_item(
        "image",
        x=img,
        rows=row,
        cols=col,
        bounds_min=(dpg.get_value("roi_l") - 0.5, dpg.get_value("roi_b") - 0.5),
        bounds_max=(dpg.get_value("roi_r") + 0.5, dpg.get_value("roi_t") + 0.5),
    )
    dpg.set_axis_limits_auto("xaxis")
    dpg.set_axis_limits_auto("yaxis")
    return


